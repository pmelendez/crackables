#include <iostream>

static int truecode = 1095520072;

int main()
{
    void* p = &truecode;
    char* c = reinterpret_cast<char*>(p);
    std::string pass(c);

    std::string password;
    std::cin >> password;

    if(password == pass)
    {
        std::cout << "Succeed" << std::endl;
    }
    else
    {
        std::cout << "Password Invalid" << std::endl;
    }

    return 0;
}
